# Terraformインストール（Linux）

インストール手順を2つ紹介いたします。いずれか実行いただければ問題ありません。

- [Terraformインストール（Linux）](#terraformインストールlinux)
  - [バイナリを用いる場合](#バイナリを用いる場合)
  - [tfenvを用いる場合](#tfenvを用いる場合)

versionは2020/12/10現在のEponaが利用するバージョンv0.13.5を利用します。

## バイナリを用いる場合

[こちら](https://releases.hashicorp.com/terraform/0.13.5/)から、Terraformのバイナリ(zip圧縮)をダウンロードしてください。

zipファイルを解凍し、terraformファイルを任意のディレクトリにコピーします。

お好みのコマンドラインツールを起動して、上記ディレクトリにパスを通してください。

```shell script
$ echo $PATH
```

以下が確認できればOKです。

```shell script
$ terraform --version
Terraform v0.13.5
```

## tfenvを用いる場合

tfenvを利用するとTerraformのバージョン変更が楽になります。

[tfenv installation](https://github.com/tfutils/tfenv#installation)

tfenvがインストールされたことを確認します。

```shell script
$ tfenv -v
```

tfenvを使ってTerraformをインストールします。

```shell script
$ tfenv install 0.13.5
```

どのバージョンを利用するか指定します。

```shell script
$ tfenv use 0.13.5
```

以下が確認できればOKです。

```shell script
$ terraform --version
Terraform v0.13.5
```
